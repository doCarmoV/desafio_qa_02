#language: pt
@post_get
Funcionalidade: Validar comportamento da API sobre as requisicoes

@post
Cenario: Realizar requisicao POST validando status code e o response do servico
    Dado que realizo um post na api de atividades com uma nova tarefa
    Entao valido que a atividade foi criada com sucesso

@get
Cenario: Realizar requisicao GET validando status code
    Dado que realizo um get na api 
    Quando valido o status code
    Entao visualizo o resultado esperado com completed true