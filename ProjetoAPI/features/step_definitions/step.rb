Dado("que realizo um post na api de atividades com uma nova tarefa") do
    url = "http://fakerestapi.azurewebsites.net/api/Activities"
    payload = Payload.new.payload_post
    header = Header.new.header_default
    response = RestClient::Request.execute(method: :post, url: url, payload: payload.to_json, headers: header)

    Validacao.new.valida_status_code(response, 200)

    @body = JSON.parse(response.body)  
end


Entao("valido que a atividade foi criada com sucesso") do  
    id_recebido = @body['ID']
    titulo_recebido = @body['Title']    

    raise "id retornado invalido" if id_recebido != $id.to_i
    raise "titulo diferente do enviado" if titulo_recebido != $titulo
end


Dado("que realizo um get na api") do
    RestClient.get('https://jsonplaceholder.typicode.com/todos') do |result|
      raise "Status code invalido #{result.code}" if result.code != 200
      @result = result.code
      body = JSON.parse(result.body)
      cont = 0
      @array = Array.new 
      while cont < 200
          if body[cont]["completed"] == true
            @array << body[cont]
              cont+=1
          else
              cont+=1 
          end
      end      
    end
end
  
Quando("valido o status code") do
    puts @result
end
  
Entao("visualizo o resultado esperado com completed true") do
    puts @array
end