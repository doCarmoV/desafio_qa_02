require 'report_builder'

ReportBuilder.configure do |config|
  config.json_path = 'data/reports/report.json'
  config.report_path = "data/reports/report"
  config.report_types = [:html]
  config.report_title = "API_Requisicoes"
  config.color = "blue"
end

Before do |scn|
  @header = Header.new
  @payload = Payload.new
  @validacao = Validacao.new
end

at_exit do
  ReportBuilder.build_report
end