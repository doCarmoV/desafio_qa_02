class Payload
    require 'ffaker'

    def payload_post
        $id = FFaker::Address.building_number
        $titulo = FFaker::NameBR.name

        payload =
        {
            "ID": "#{$id}",
            "Title": "#{$titulo}",
            "DueDate": "2019-02-10T17:50:47.837Z",
            "Completed": true
        }

        payload
    end

end