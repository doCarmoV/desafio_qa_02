class Validacao

    def valida_status_code(response, status_esperado)
        if response.code != status_esperado.to_i
            raise "Status code esperado: #{status_esperado}, status code retornado, #{response.code}, ERRO."
        end
    end

end