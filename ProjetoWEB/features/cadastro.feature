#language: pt

Funcionalidade: Cadastrar novo Fornecedor 

Contexto: Logar em phptravels
  Dado que estou na area logada do site phptravels

  @cadastro
Cenario: Validar cadastro de um novo Fornecedor
  E clico em Suppliers pelo menu Accounts
  Quando realizo o cadastro de um novo fornecedor
  Entao espero validar que o mesmo foi adicionado a lista de fornecedores
