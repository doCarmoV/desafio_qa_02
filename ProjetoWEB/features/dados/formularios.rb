require 'ffaker'

FORMULARIO = {
  add_supplier: {
    first_name: FFaker::NameBR.first_name,
    last_name: FFaker::NameBR.last_name,
    email: FFaker::Internet.safe_email,
    password: FFaker::Internet.password,
    m_number: FFaker::PhoneNumberBR.mobile_phone_number,
    address1: FFaker::AddressBR.street,
    address2: FFaker::AddressBR.street,
    name: FFaker::NameBR.first_name
  }
}.freeze
