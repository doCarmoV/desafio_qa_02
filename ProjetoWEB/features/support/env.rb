require 'rspec'
require 'capybara/cucumber'
require 'pry'
require 'yaml'
require 'ffaker'
require 'selenium/webdriver'
require 'report_builder'


EL = YAML.load_file('data/Yaml/elementos.yml')

if ENV['chrome']
  $browser = "Google Chrome"
  Capybara.default_driver = :chrome
  Capybara.register_driver :chrome do |app|
  client = Selenium::WebDriver::Remote::Http::Default.new 
  client.timeout = 200
    Capybara::Selenium::Driver.new(app, browser: :chrome, switches:['--disable-infobars','--disable-print-preview', 'start-maximized'], :http_client => client)
  end
else ENV['chrome_headless']
  $browser = "Google Chrome Headless"
  Capybara.default_driver = :chrome_headless
  Capybara.register_driver :chrome_headless do |app|
    client = Selenium::WebDriver::Remote::Http::Default.new
    client.timeout = 200
    Capybara::Selenium::Driver.new(app,browser: :chrome_headless,args: ['--disable-setuid-sandbox','--headless', '--no-sandbox', '--disable-dev-shm-usage'], :http_client => client)
  end
end

Capybara.default_max_wait_time = 30