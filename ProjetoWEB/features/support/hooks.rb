require 'selenium-webdriver'
require 'report_builder'

Before do |_scn|
  @Comum = Comum.new
  @Inicio = Inicio.new
  @Cadastro = Cadastro.new
  @Validacoes = Validacoes.new

  Capybara.current_session.driver.browser.manage.delete_all_cookies
end

After do |scn|
    file_name = scn.name.gsub(' ', '_').downcase!
    target = "data/screenshots/#{file_name}.png"
    if scn.failed?
      page.save_screenshot(target)
      embed(target, 'image/png', 'Evidência')
    else
      page.save_screenshot(target)
      embed(target, 'image/png', 'Evidência')
    end
    page.driver.quit
end
  
at_exit do 
  data = Time.now.strftime('%d_%B_%y')
  ReportBuilder.configure do |config|
  config.json_path = 'data/reports/Relatorio_de_execucao.json'
  config.report_path = "data/reports/Report_#{data}"
  config.report_types = [:html]
  config.report_title = 'Relatório de Execucao'
  config.color = 'red'
  config.include_images = true
end
  ReportBuilder.build_report
end
  