class Cadastro
    include Capybara::DSL

    def preencher_login
        dados = CONTA[:login_supplier]

        find(EL["login"]["campo_email"]).set(dados[:login])
        find(EL["login"]["campo_senha"]).set(dados[:password])
        find(EL["login"]["btn_entrar"]).click
        has_no_text?("Redirecting please wait")
    end    

    def cadastrar_fornecedor
        dados = FORMULARIO[:add_supplier]

        find(EL["logado"]["btn_add"]).click

        find(EL["formulario"]["campo_pnome"]).set(dados[:first_name])
        find(EL["formulario"]["campo_snome"]).set(dados[:last_name])
        $mail = (dados[:email])
       
        find(EL["formulario"]["campo_email"]).set("#{$mail}")
        find(EL["formulario"]["campo_senha"]).set(dados[:password])
        find(EL["formulario"]["campo_nmobile"]).set(dados[:m_number])

        find(EL["formulario"]["campo_pais"]).click
        all(EL["formulario"]["lista_pais"])[3].set("Brazil").native.send_keys(:enter)

        find(EL["formulario"]["campo_endereco1"]).set(dados[:address1])
        find(EL["formulario"]["campo_endereco2"]).set(dados[:address2])

        find(EL["formulario"]["lista_status"]).click.find(EL["formulario"]["valor_desabilitado"]).select_option
        find(EL["formulario"]["lista_tp_fornecedor"]).click.find(EL["formulario"]["valor_tours"]).select_option

        find(EL["formulario"]["campo_nome"]).set(dados[:name])

        first(EL["formulario"]["slct_newsletter"]).click

        first(EL["formulario"]["campo_atrib_hotel"]).click
        assert_selector(EL["formulario"]["campo_hotel"])
        find(EL["formulario"]["lista_hotel"]).set("rose").native.send_keys(:enter)

        find(EL["formulario"]["btn_enviar"]).click
    end    
end    