class Inicio
    include Capybara::DSL
  
    def acessar_url
      visit("https://www.phptravels.net/admin")
      assert_selector(EL["login"]["lbl_login"])
    end
  
end