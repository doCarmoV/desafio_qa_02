class Validacoes
    include Capybara::DSL

    def validar_cadastro_fornecedor
        assert_selector(EL["logado"]["btn_add"])
        raise "Fornecedor #{$mail} nao cadastrado" if find(".panel-body").assert_text("#{$mail}") == false
    end    
end    