# **Desafio QA**

---

## Repositório testes automatizados

Repositório de testes criado com conteúdo por Rodrigo Vieira

---

## Instalação

Para executar o projeto é necessário ter o Ruby instalado na máquina. Linux e MacOs vem por padrão com o Ruby instalado, para Windows basta baixar o (https://rubyinstaller.org/)
É necessário instalar a gem Bundler (https://rubygems.org/gems/bundler/versions/1.16.2)

gem install bundler

Dentro da pasta do projeto de testes executar o comando:

bundle install


---

## Uso

WebSite:
cucumber chrome=true -t @cadastro

WebService:
cucumber -t @post_get


---

## Report

O report se encontrará em:

data/reports/Relatorio_de_execucao
